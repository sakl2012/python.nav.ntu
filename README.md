# [2019 Fall] An Introduction to Python Programming
歡迎參加本 Python 的秋季班課程，你可以在這裡找到所有的資源。若有問題可以到[討論看板][board]留言。

## 課堂範例
* [Lecture 1: Syntax][Lec1example]
* [Lecture 2: Matplotlib][Lec2example]
* [Lecture 3: Numpy][Lec3example]
* [Lecture 4: File Handling][Lec4example]

### 補充資料

## 作業練習
* [作業一][Homework1]

## 課堂錄影
* [2019.09.14][20190914VideoYT]
* [2019.09.21][20190921VideoYT]
* [2019.09.28][20190928VideoYT]
* [2019.10.05][20191005VideoYT]
* [2019.10.26][20191026VideoYT]

[board]: https://trello.com/invite/b/BUABjVDQ/5ca37e0a7c9ed462a292c0e73ccacacf/pythonnavntu

[Lec1example]: https://colab.research.google.com/drive/1wRzpRD5xaQXZcFDEljgAJsNMUVlQxGEn
[Lec2example]: https://colab.research.google.com/drive/116r9YwjSb1UKdjOU6hMGtbemj_MVdRf-
[Lec3example]: https://colab.research.google.com/drive/1sE9QC6V0Z6FGyuorL0f9rNIjaKu8FV1c
[Lec4example]: https://colab.research.google.com/drive/11CY6HG87tpKdmrqplcdjPk5a2CngtrJC

[Homework1]: https://colab.research.google.com/drive/1XL_qe_M1xrnWtGQgAtEBe45W77AMJHfq
[Homework1answer]: https://colab.research.google.com/drive/1lIsrYCl3-DdZsaC8Ep_GLc-JHquuFZ7G

[20190914VideoYT]: https://youtu.be/gmYE_ooQn8U
[20190921VideoYT]: https://youtu.be/y6ONtfJq6ZA
[20190928VideoYT]: https://youtu.be/cUYyK0O-mdA
[20191005VideoYT]: https://youtu.be/MUVDhhhfXQI
[20191026VideoYT]: https://youtu.be/vxqkmwYgZxM

[FileRequest]: https://www.dropbox.com/request/hJTSbLZBtwCEevvZ57gk